Bacon-Cape-Test
===============

1. Setting up autologin on ttyO0

Copy autologin@.service to /etc/systemd/system/ and run:

ln -s /etc/systemd/system/autologin@.service /etc/systemd/system/getty.target.wants/serial-getty@ttyO0.service


Copy autologin.sh to /home/debian then make it executable.

Edit autologin@.service by updating the ExecStart command:

ExecStart=-/sbin/agetty -n -l /home/debian/autologin.sh -s %I 115200,38400,9600


2. Copy test files

Copy Bacon-test.sh to /usr/bin/ and make it executable

Copy bacon-test.service to /etc/systemd/system

Enable and start service
